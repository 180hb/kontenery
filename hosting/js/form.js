$(document).ready(function () {
  const form = $(document).find('#wf-form-Contact-Form');

  form.on('submit', function (event) {
    $.post({
      url: form.attr('action'),
      crossDomain: true,
      data: form.serialize(),
      beforeSend: function () {
        $('#form-fail, #form-done').hide();
      },
      error: function () {
        $('#form-fail').show();
      },
      complete: function (response) {
        if (response.responseText == 'done') {
          $('#form-done').show();
        } else {
          $('#form-fail').show();
        }
      },
    });

    event.preventDefault();
  });
});
