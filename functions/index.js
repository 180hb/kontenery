const functions = require("firebase-functions");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    host: '180heartbeats.home.pl',
    port: 587,
    auth: {
        user: 'kontakt@konteneryeventowe.pl',
        pass: '8qREUMY6',
    },
    tls: {
        rejectUnauthorized: false,
    },
});

exports.webflowMail = functions.https.onRequest((request, response) => {
    let origin = request.headers.origin;

    if (['https://kontenery-99ae3.web.app', 'https://konteneryeventowe.pl'].includes(origin)) {
        response.set('Access-Control-Allow-Origin', origin);
    }

    const options = {
        from: request.body.name + ' ' + request.body.surname + ' kontakt@konteneryeventowe.pl',
        replyTo: request.body.email,
        to: 'kontakt@konteneryeventowe.pl',
        subject: 'konteneryeventowe.pl - Formularz',
        html: request.body.message + '<br><br>' + request.body.name + ' ' + request.body.surname + '<br>' + request.body.email + '<br>' + request.body.phone,
    };

    transporter.sendMail(options, function (error, info) {
        if (error) {
            response.send('fail');
        } else {
            response.send('done');
        }
    });
});
